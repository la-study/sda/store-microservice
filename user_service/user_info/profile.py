from .models import User
from .forms import UserForm
def retrieve(request): 
  try:
    profile = request.user.userprofile
  except User.DoesNotExist:
    profile = User(user=request.user)
    profile.save()
  return profile

def set(request):
  profile = retrieve(request)
  profile_form = UserForm(request.POST, instance=profile)
  profile_form.save() 