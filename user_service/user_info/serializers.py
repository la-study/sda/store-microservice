from rest_framework import serializers
from rest_framework_simplejwt.tokens import RefreshToken
from .models import User
import json


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = "__all__"
        extra_kwargs = {'password': {'write_only': True}}

    def validate(self, attrs):
        if User.objects.filter(username=attrs['username']).exists():
            raise serializers.ValidationError(
                {'username': 'Username already exists'})
        if User.objects.filter(email=attrs['email']).exists():
            raise serializers.ValidationError(
                {'email': 'Email already exists'})
        return attrs

    def create(self, validated_data):
        user = User.objects.create_user(
            username=validated_data['username'],
            email=validated_data['email'],
            password=validated_data['password']
        )
        return user
