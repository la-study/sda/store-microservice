# Generated by Django 4.1.6 on 2023-04-03 10:08

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=100)),
                ('username', models.CharField(default='', max_length=100, unique=True)),
                ('email', models.EmailField(default='', max_length=100, unique=True)),
                ('name', models.CharField(default='', max_length=100)),
                ('isAdmin', models.BooleanField(default=False)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
