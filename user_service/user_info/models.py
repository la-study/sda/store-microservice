from django.db import models
from django.contrib.auth.models import User


class User(User):
    # Delete not use field
    # email = None
    last_login = None
    is_staff = None
    is_superuser = None

    isAdmin = models.BooleanField(default=False)

    REQUIRED_FIELDS = ["username", "email", "first_name", "last_name"]


    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name
