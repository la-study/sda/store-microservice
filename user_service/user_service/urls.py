from django.contrib import admin

from django.urls import path, include
from user_info import views
from .views import order_views, image_views, book_views, comment_views


urlpatterns = [
    #   path('', include('rest_framework.urls')),
    path('admin/', admin.site.urls),
    path('register/', views.register, name="register"),
    path('login/', views.MyTokenObtainPairView.as_view(), name="login"),
    path('profile', views.getUserProfile, name="my_account"),
    path('order_info/', views.order_info, name="order_info"),
    path('cart', include('user_cart.urls'), name="order_info"),

    path('orders',  order_views),
    path('books',  book_views),
    path('upload-image', image_views),
    path('comments',  comment_views),
]
