import json

import requests
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from user_info.serializers import UserSerializer


@ api_view(['GET', 'POST'])
@ permission_classes([IsAuthenticated])
def order_views(request):
    if request.method == 'GET':
        url = 'http://127.0.0.1:3003/orders?user_id=' + str(request.user.id)
        headers = {'Content-Type': 'application/json'}
        response = requests.get(url, headers=headers)
        res = json.loads(response.content.decode('utf-8'))
        return Response(res, status=status.HTTP_200_OK)

    elif request.method == 'POST':
        req = json.loads(request.body)
        req["user"] = UserSerializer(request.user).data
        url = 'http://127.0.0.1:3003/orders'
        headers = {'Content-Type': 'application/json'}
        response = requests.post(url, json=req, headers=headers) 
        res = json.loads(response.content.decode('utf-8')) 

        return Response(res, status=status.HTTP_201_CREATED)


@ api_view(['GET', 'POST'])
# @ permission_classes([IsAuthenticated])
def book_views(request):
    if request.method == 'GET':
        url = 'http://127.0.0.1:3005/books'
        headers = {'Content-Type': 'application/json'}
        response = requests.get(url, headers=headers)
        res = json.loads(response.content.decode('utf-8'))
        return Response(res, status=status.HTTP_200_OK)

    elif request.method == 'POST':
        req = json.loads(request.body)
        req["user"] = UserSerializer(request.user).data
        url = 'http://127.0.0.1:3005/books'
        headers = {'Content-Type': 'application/json'}
        response = requests.post(url, json=req, headers=headers)
        res = json.loads(response.content.decode('utf-8'))

        return Response(res, status=status.HTTP_201_CREATED)


@ api_view(['GET', 'POST'])
@ permission_classes([IsAuthenticated])
def image_views(request):
    image = request.FILES.get('image')
    url = 'http://127.0.0.1:8006/upload-image'
    files = {'image': image}
    response = requests.post(url, files=files)
    res = json.loads(response.content.decode('utf-8'))
    return Response(res, status=response.status_code)


@ api_view(['GET', 'POST'])
@ permission_classes([IsAuthenticated])
def comment_views(request):
    if request.method == 'GET':
        url = 'http://127.0.0.1:3007/comments?product_id=' + \
            str(request.query_params["product_id"])

        print("url: ", url)
        headers = {'Content-Type': 'application/json'}
        response = requests.get(url, headers=headers)
        res = json.loads(response.content.decode('utf-8'))
        return Response(res, status=status.HTTP_200_OK)

    elif request.method == 'POST':
        req = json.loads(request.body)
        req["owner_id"] = UserSerializer(request.user).data["id"]
        url = 'http://127.0.0.1:3007/comments'
        headers = {'Content-Type': 'application/json'}
        response = requests.post(url, json=req, headers=headers)
        print("response: ", response)
        res = json.loads(response.content.decode('utf-8'))
        print("response: ", res)

        return Response(res, status=status.HTTP_201_CREATED)
