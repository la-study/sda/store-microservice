import random

from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from user_info.models import User

from .models import CartItem
from .serializers import CartItemSerializer


def get_cart_items(request):
    return CartItem.objects.filter(owner=request.user.id)


# return all items from the current user's cart
@ api_view(['GET', 'POST'])
@ permission_classes([IsAuthenticated])
def cartView(request):
    if request.method == 'GET':
        cart_items = get_cart_items(request)
        serializer = CartItemSerializer(cart_items, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    elif request.method == 'POST':
        postData = request.data
        product_id = postData.get('product_id', -1)
        quantity = postData.get('quantity', 1)

        # fetch the product or return a missing page error

        # get products in cart
        cart_products = get_cart_items(request)
        product_in_cart = False

        # check to see if item is already in cart
        for cart_item in cart_products:
            if cart_item.product.id == product_id:
                product_in_cart = True
                if postData.get('minusCheck'):
                    cart_item.decrease_quantity(quantity)
                else:
                    cart_item.augment_quantity(quantity)
            return Response({"message": "Add to cart successfully"}, status=status.HTTP_201_CREATED)

        if not product_in_cart and not postData.get('minusCheck'):
            # create and save a new cart item
            ci = CartItem()
            ci.product = product_id
            ci.quantity = quantity
            ci.owner = request.user.id
            ci.save() 
            return Response({"message": "Add to cart successfully"}, status=status.HTTP_201_CREATED)
