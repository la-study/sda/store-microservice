from django.db import models
from user_info.models import User


class CartItem(models.Model):
    date_added = models.DateTimeField(auto_now_add=True)
    quantity = models.IntegerField(default=1)
    product = models.IntegerField(null=False)
    # owner = models.ForeignKey(
    #     User, null=False, blank=False, on_delete=models.CASCADE, default=1)
    owner = models.IntegerField(null=False)

    class Meta:
        db_table = 'cart_items'
        ordering = ['date_added']

    def total_price(self):
        return self.quantity * self.product.price

    def name(self):
        return self.product.name

    def product_image(self):
        return self.product.image

    def price(self):
        return self.product.price

    def get_absolute_url(self):
        return self.product.get_absolute_url()

    def augment_quantity(self, quantity):
        self.quantity = self.quantity + int(quantity)
        self.save()

    def decrease_quantity(self, quantity):
        if self.quantity == 1:
            # delete record
            self.delete()
        else:
            self.quantity = self.quantity - int(quantity)
            self.save()
