# Generated by Django 4.1.6 on 2023-04-20 00:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('comment_model', '0005_alter_comment_content_alter_comment_owner_id_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment',
            name='owner_id',
            field=models.IntegerField(),
        ),
        migrations.AlterField(
            model_name='comment',
            name='product_id',
            field=models.IntegerField(),
        ),
    ]
