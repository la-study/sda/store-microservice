# Generated by Django 4.1.6 on 2023-04-19 23:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('comment_model', '0003_rename_user_id_comment_owner_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='comment',
            name='image',
            field=models.CharField(default='', max_length=255),
        ),
    ]
