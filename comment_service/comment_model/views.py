from django.shortcuts import render
from rest_framework import status

import json
import requests
from rest_framework.decorators import api_view
from rest_framework.response import Response

from .models import Comment
from .serializer import CommentSerializer

# Create your views here.


@api_view(['GET', 'POST'])
def comment_views(request):
    if request.method == 'GET':
        comments = Comment.objects.filter(
            product_id=request.query_params["product_id"]
        )
        serializers = CommentSerializer(comments, many=True)
        return Response(serializers.data, status=status.HTTP_200_OK)

    elif request.method == 'POST':
        req_body = json.loads(request.body)

        product_id = req_body["product_id"]
        owner_id = req_body["owner_id"]
        image = req_body.get("image", "")
        content = req_body.get("content", "")
        rating = req_body.get("rating", 0)

        if product_id and owner_id and rating:
            comment = Comment.objects.create(
                product_id=int(product_id),
                owner_id=int(owner_id),
                image=image,
                content=content,
                rating=rating,
            )

            comment.save()
            serializer = CommentSerializer(comment, many=False)
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        else:
            return Response({"message": "Fill all required fields"}, status=status.HTTP_400_BAD_REQUEST)
