from django.db import models

# Create your models here.


class Comment(models.Model):
    product_id = models.IntegerField(null=False)
    owner_id = models.IntegerField(null=False)
    image = models.CharField(max_length=255, default="")
    content = models.CharField(max_length=500, default="")
    rating = models.IntegerField(default=0)

    class Meta:
        db_table = 'comments'
        verbose_name_plural = 'Comments'
