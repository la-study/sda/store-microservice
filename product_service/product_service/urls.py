
from django.contrib import admin
from django.urls import path
from product_models.views import ProductListView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('products', ProductListView.as_view()),
]
