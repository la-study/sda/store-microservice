from .models import Product
from .serializers import ProductSerializer, PreviewProductSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from django.views.generic.edit import CreateView
from rest_framework import status


class ProductListView(APIView):
    def get_queryset(self):
        return Product.objects.all()

    def get(self, request, format=None):
        if(request.query_params["id"]):
            products = Product.objects.filter(id=int(request.query_params[id]))
        else:
            products = Product.objects.all()
        if(request.query_params["is-short"] == 'true'): 
            serializer = PreviewProductSerializer(products, many=True)
        else:
            serializer = ProductSerializer(products, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class ProductCreateView(CreateView):
    model = Product
    fields = ['name', 'description', 'price']

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super().form_valid(form)
