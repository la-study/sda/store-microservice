from django.contrib import admin
from django.urls import path
from book_model import views
urlpatterns = [
    path('admin/', admin.site.urls),
    path('books', views.book_views),
    path('create-book/', views.createBook),
    path('get-book/', views.getBookById),
]
