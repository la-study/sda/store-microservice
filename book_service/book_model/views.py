import json

import requests
from book_model.models import Book
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import BookSerializer
from rest_framework import status


@api_view(['GET', 'POST'])
def book_views(request):
    if request.method == 'GET':
        orders = Book.objects.all()
        serializers = BookSerializer(orders, many=True)
        return Response(serializers.data, status=status.HTTP_200_OK)

    elif request.method == 'POST':
        req_body = json.loads(request.body)
        image = req_body['image']
        name = req_body['name']
        description = req_body['description']
        author = req_body['author']
        price = req_body['price']

        if image and name and author and description and price:
            # call image_service to store image into db
            book = Book.objects.create(
                name=name,
                image=image,
                description=description,
                author=author,
                price=price,
            )
            book.save()
            serializer = BookSerializer(book, many=False)

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response({"message": "Fill all required fields"}, status=status.HTTP_400_BAD_REQUEST)

    Response({"message": "Fill all required fields"},
             status=status.HTTP_400_BAD_REQUEST)


@csrf_exempt
def createBook(request):
    image = request.FILES.get('image')
    id = request.POST.get('id')
    name = request.POST.get('name')
    author = request.POST.get('author')
    availability = request.POST.get('availability')
    description = request.POST.get('description')
    price = request.POST.get('price')

    resp = {}
    if image and id and name and author and availability and description and price:
        # call image_service to store image into db
        data = {}
        data["ProductId"] = id
        url = 'http://127.0.0.1:5005/upload-product-img/'
        files = {'image': image}
        response = requests.post(url, data=data, files=files)
        rs = json.loads(response.content.decode('utf-8'))
        print(response)
        book = Book()
        book.id = id
        book.name = name
        book.author = author
        book.availability = availability
        book.description = description
        book.price = price
        book.save()

        res = book.to_json()
        res['image_path'] = rs['path']
        resp['status'] = "Success"
        resp['status_code'] = '200'
        resp['data'] = res
    else:
        resp['status'] = "Failed"
        resp['status_code'] = '400'
        resp['message'] = 'All fields are mandatory'
    return HttpResponse(json.dumps(resp), content_type='application/json')


@csrf_exempt
def getBookById(request):
    req = json.loads(request.body)
    book_id = req['product_id']
    bookdata = Book.objects.all().filter(id=book_id)
    data = []
    for value in bookdata.values():
        data.append(value)
    resp = {}
    resp['data'] = data
    return HttpResponse(json.dumps(resp), content_type='application/json')
