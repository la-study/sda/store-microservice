from django.db import models
from static.utils.create_slug import create_slug

# Create your models here.


class Book(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=500, unique=True, default="",
                            help_text='Unique value for product page URL, created from name.', null=False)
    image = models.CharField(max_length=255, default="")
    description = models.CharField(max_length=100)
    price = models.IntegerField()

    author = models.CharField(max_length=100)
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = 'books'
        verbose_name_plural = 'Books'

    def to_json(self):
        return {
            "id": self.id,
            "name": self.name,
            "author": self.author,
            "availability": self.availability,
            "description": self.description,
            "price": self.price
        }

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = create_slug(self.name, Book)
        # important to call this
        return super(Book, self).save(*args, **kwargs)
