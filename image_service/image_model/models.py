from django.db import models


class Image(models.Model):
    path = models.CharField(max_length=1000, default="")

    def to_json(self):
        return {
            "path": self.path
        }

    class Meta:
        db_table = 'images'
        verbose_name_plural = 'Images'
