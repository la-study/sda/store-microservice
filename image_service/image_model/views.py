import os

from django.conf import settings
from image_model.models import Image
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from static.utils.create_slug import convert_path


@api_view(['POST'])
def updateProductImage(request):
    file = request.FILES.get('image')

    if file:
        # Tao duong dan
        image_dir = 'static/upload'
        if not os.path.exists(image_dir):
            os.makedirs(image_dir)

        # path toi file
        image_path = os.path.join(image_dir, convert_path(file.name))

        # Ghi file vao o dia
        with open(image_path, 'wb+') as f:
            for chunk in file.chunks():
                f.write(chunk)

        image = Image()
        image.path = image_path
        image.save()

        return Response({"message": 'Upload image successfully',  "path": image_path},
                        status=status.HTTP_201_CREATED
                        )
    else:
        return Response({"message": 'No image found'}, status=status.HTTP_400_BAD_REQUEST)
