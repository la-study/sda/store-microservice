from django.utils.text import slugify
from static.translate.vietnamese_map import vietnamese_map


def create_slug(name, model, new_slug=None):
    slug = slugify(name, allow_unicode=True).translate(vietnamese_map)
    if new_slug is not None:
        slug = new_slug.translate(vietnamese_map)
    qs = model.objects.filter(slug=slug).order_by("-id")
    exists = qs.exists()
    if exists:
        new_slug = "%s-%s" % (slug, qs.first().id)
        return create_slug(name, model, new_slug=new_slug).translate(vietnamese_map)
    return slug


def convert_path(path):
    slug = slugify(path, allow_unicode=True).translate(vietnamese_map)
    return slug
