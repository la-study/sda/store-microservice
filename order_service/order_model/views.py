import json
import urllib.parse
import requests

from django.http import HttpResponseBadRequest
from order_model.models import Order, OrderItem
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from .serializers import OrderSerializer, OrderItemSerializer


@api_view(['GET', 'POST'])
def order_views(request):
    if request.method == 'GET':
        orders = Order.objects.filter(owner=request.GET["user_id"])

        serializers = OrderSerializer(orders, many=True)

        for order in serializers.data:
            orderItem = OrderItem.objects.filter(order=order["id"])
            orderItemSerializer = OrderItemSerializer(orderItem[0])
            order["items"] = orderItemSerializer.data

        return Response(serializers.data, status=status.HTTP_200_OK)

    elif request.method == 'POST':
        try:
            # Parse the request body as JSON
            req = json.loads(request.body)
            items = req['items']
            address = req['address']
            full_name = req['full_name'] 
            note = req['note']

            if full_name and address and items:
                if len(items) <= 0:
                    return Response({"message": "No product selected"}, status=status.HTTP_400_BAD_REQUEST)

                order = Order()
                order.owner = req["user"]["id"]
                order.full_name = full_name
                order.address = address
                if note:
                    order.note = note
                order.save()

                for item in items:
                    order_item = OrderItem.objects.create(
                        product_id=item["id"],
                        product_name=item["name"],
                        price=item["price"],
                        quantity=item["quantity"],
                        order=order
                    )
                    order_item.save()

                serializer = OrderSerializer(order, many=False)
                return Response(serializer.data, status=status.HTTP_201_CREATED)

            else:
                return Response({"message": "Fill all required field"}, status=status.HTTP_400_BAD_REQUEST)
        except json.JSONDecodeError:
            # Return a bad request response if the JSON is invalid
            return HttpResponseBadRequest('Invalid JSON')
        # req = urllib.parse.parse_qs(request.body.decode('utf-8'))
