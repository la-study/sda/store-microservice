from django.db import models


class Order(models.Model):
    owner = models.IntegerField(null=False)
    full_name = models.CharField(max_length=100)
    address = models.CharField(null=False, default="", max_length=500)
    note = models.TextField(default="")
    is_paid = models.BooleanField(default=False)
    status = models.IntegerField(default=0)
    # AWAITING_CONFIRM, AWAITING_SHIPMENT, SHIPPED, CANCELLED

    class Meta:
        db_table = 'orders'
        verbose_name_plural = 'Order'


class OrderItem(models.Model):
    product_id = models.CharField(max_length=50)
    product_name = models.CharField(max_length=500)
    price = models.BigIntegerField()
    quantity = models.IntegerField()
    order = models.ForeignKey(Order, null=False,
                              blank=False, on_delete=models.CASCADE, default=1)

    class Meta:
        db_table = 'order_items'
        verbose_name_plural = 'OrderItem'

    def __unicode__(self):
        return self.product_name

    def __str__(self):
        return self.product_name
