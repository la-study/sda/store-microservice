from django.contrib import admin
from django.urls import path
from clothe_model import views
urlpatterns = [
    path('admin/', admin.site.urls),
    path('create-clothe/', views.createClothe),
    path('get-clothe/', views.getClotheById),
]
